;Flips characters on screen from left to right

;----------------------------------------------------------------------- STACK SEGMENT
MyStack SEGMENT STACK              

        DW 256 DUP (?)             ; At least a small stack is needed for interrupt
                                   ; handling.

MyStack ENDS                       ; End Segment

;------------------------------------------------------------------------ DATA SEGMENT
MyData SEGMENT
    
    VideoMem EQU 0B800h           ; Segment for video RAM.

MyData ENDS                        ; End Segment

;------------------------------------------------------------------------ CODE SEGMENT

MyCode SEGMENT
        ASSUME CS:MyCode, DS:MyData    ; Tell the assembler what segments the segment 
                                       ; registers refer to in the following code.

;------------------------------------------------------------------------- MAIN

Main	  PROC                         ; User Transfer Address

	MOV     AX, MyData             ; Make DS address our data segment.  Two statements
	MOV     DS, AX                 ; are required because it is illegal to move
                                       ; immediate data directly into a segment register.
                                       ; This is essential if your program refers to any
                                       ; of symbols declared with DB, DW, or DD in the
                                       ; data segment of your program.

	MOV     AX, VideoMem           ; Make ES segment register address video memory.
	MOV     ES, AX

    	MOV	SI, 0			; Makes SI an offset to the far left of the screen
    	MOV	DI, 158			; Makes DI an offset to the far right of the screen
    		                               
	CALL FlipRow		      ; Flips text on screen
				      ; Saves SI, AX, and DI
		
	MOV     AH, 4Ch                ; These two instructions use a DOS interrupt
	INT     21h                    ; to release the memory for this program and
                                       ; then return control to DOS.  This is the
                                       ; logical end of the program.

Main	 ENDP                      ; End of the Main proc

;------------------------------------------------------------------------ END MAIN

;------------------------------------------------------------------------ FLIP ROW	

FlipRow	   PROC			;On entry, SI and DI point 
				;to the first row of video memory
	
	PUSH AX SI DI
	MOV CX, 40							;Set CX to the number of column loop iterations				
	MOV DX, 25 * 160						;Set DX to the number of row loop iterations
	
	rowLoop:
	
	 	columnLoop:
	 			
			MOV AX,ES:[SI]					;Get character from left column
			MOV BX,ES:[DI]					;Get character from right column
				
									;Here BL = character from right half of screen			
			CMP BL, 'A' 					;if BL is < 'A'
			JL redFirstLetter					;Jump to red text
			CMP BL, 'Z'					;else if BL <= 'Z' 
			JLE dontChangeFirstLetter				;don't change color
			CMP BL, 'a'					;else if BL is < 'a'
			JL redFirstLetter					;Jump to red text
			CMP BL, 'z'					;else if BL <= 'z' 
			JLE dontChangeFirstLetter				; don't change color
			JMP redFirstLetter				;else
										;Jump to red text
						
			redFirstLetter:
				MOV BH, 04h				; Set red text
				
			dontChangeFirstLetter:				;Was an alphabetic character
			
								;Here AL = character from left half of screen
			CMP AL, 'A' 					;if AL is < 'A'
			JL redSecondLetter					;Jump to red text
			CMP AL, 'Z'					;else if AL <= 'Z' 
			JLE dontChangeSecondLetter				;don't change color
			CMP AL, 'a'					;else if AL is < 'a'
			JL redSecondLetter					;Jump to red text
			CMP AL, 'z'					;else if AL <= 'z' 
			JLE dontChangeSecondLetter				; don't change color
			JMP redSecondLetter				;else
										;Jump to red text
					
			redSecondLetter:
				MOV AH, 04h				;Set red text
				
			dontChangeSecondLetter:				;Was an alphabetic character
		
		 	MOV ES:[SI], BX					;Swap characters
		 	MOV ES:[DI], AX 
		 	
			DEC CX					;columnCounter--
			ADD SI, 2				;Move to next character in video memory
			SUB DI, 2				;Move to next character in video memory from right 
  		 	CMP CX, 0 				;Are SI and DI in the middle column of video memory?
  		 	JG columnLoop				
  		 		
  		MOV CX,40						;Reset CX for columnLoop
		ADD SI, 80						;Move SI to the next row in video memory
		ADD DI, 80 + 160					;Move DI to the next row in video memory
		CMP DX, SI						;Is SI past video memory?
		JG rowLoop
	
	 POP  DI SI AX						;Resets AX, SI, and DI to what they were before procedure
	 
		   RET			  ; Return to Main proc
	FlipRow	   ENDP			  ; End of Flip Row

;------------------------------------------------------------------------ END FLIP ROW	

MyCode ENDS                        ; End Segment

;-------------------------------------------------------------------------------------
END Main                           ; This END marks the physical end of the source code
                                   ; file.  The label specified will be the
                                   ; "user transfer address" where execution will begin.